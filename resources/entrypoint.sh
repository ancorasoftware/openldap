#!/bin/sh

# When not limiting the open file descritors limit, the memory consumption of
# slapd is absurdly high. See https://github.com/docker/docker/issues/8231
ulimit -n 8192

set -e

#
# Build templating context used for configuration and data bootstrapping
#

SLAPD_LOAD_LDIFS="structure.ldif,default-ppolicy.ldif,users.ldif,${SLAPD_LOAD_LDIFS}"
SECRETS_DIR="${SECRETS_DIR:-/run/secrets}"
SLAPD_FORCE_RECONFIGURE="${SLAPD_FORCE_RECONFIGURE:-false}"

configuration_error=false

# Validate SLAPD_DOMAIN is set 
if [ -z "$SLAPD_DOMAIN" ]; then
  configuration_errors=true
  echo >&2 "Error: SLAPD_DOMAIN not set. "
  configuration_error=true  
fi

# Render & export SLAPD_FULL_DOMAIN
if [ -z "$SLAPD_FULL_DOMAIN" ]; then
  SLAPD_FULL_DOMAIN=`echo "$SLAPD_DOMAIN" | sed 's/^/dc=/;s/\./,dc=/g'`
  export SLAPD_FULL_DOMAIN
fi
echo "SLAPD_FULL_DOMAIN = $SLAPD_FULL_DOMAIN"

# Export SLAPD_ORGANIZATION
SLAPD_ORGANIZATION="${SLAPD_ORGANIZATION:-${SLAPD_DOMAIN}}"
export SLAPD_ORGANIZATION
echo "SLAPD_ORGANIZATION = $SLAPD_ORGANIZATION"

# Hash & export slapd password secrets
if [ -n "$SLAPPASSWD_SECRETS" ]; then
  missing_secrets=false
  IFS=","
  for secret in $SLAPPASSWD_SECRETS; do 
    # Validate secret file exists
    if [ ! -f "$SECRETS_DIR/$secret" ]; then
      echo >&2 "Error: $SECRETS_DIR/$secret does not exist."
      missing_secrets=true
    fi

    # Hash and export secret
    passwd=$(slappasswd -T "$SECRETS_DIR/$secret")
    eval "$secret=$passwd";
    export $secret
  done

  if [ "$missing_secrets" = "true" ]; then
    configuration_error=true
    echo >&2 "Did you correctly configure SLAPPASSWD_SECRETS?"
  fi
fi

# Error if any configuration errors exist
if [ "$configuration_error" = "true" ]; then
  echo >&2 "Aborting due to configuration errors."
  exit 1
fi

# Render templating context from current environment.
env | sed 's/[\%]/\\&/g;s/\([^=]*\)=\(.*\)/s%\\\%\1\\\%%\2%/' > /tmp/env_tmp

#
# Configure slapd 
#

first_run=true
if [ -f "/var/lib/openldap/openldap-data/data.mdb" ]; then 
  mkdir -p /var/lib/openldap/openldap-data
  first_run=false
fi

if [ ! -d /etc/openldap/slapd.d ] || [ ! "$(ls -A "/etc/openldap/slapd.d")" ] || [ "$SLAPD_FORCE_RECONFIGURE" = "true" ] || [ "$first_run" = "true" ]; then
	echo "configuring slapd ..."

	mkdir -p /etc/openldap/slapd.d
	chmod 750 /etc/openldap/slapd.d

  # Render and apply slap.d configuration
  cat /etc/openldap.dist/slapd.conf | sed -f /tmp/env_tmp > /tmp/slapd.conf
  slapadd -f /tmp/slapd.conf
  slaptest -f /tmp/slapd.conf -F /etc/openldap/slapd.d
  rm -f /tmp/slapd.conf

  # Configure additonal modules
  if [ -n "$SLAPD_ADDITIONAL_MODULES" ]; then
    echo "Configuring additional modules..."
    for module in $SLAPD_ADDITIONAL_MODULES; do 
      echo "Loading module '$module' ..."
      slaptest -u
      tmp_module_file="/tmp/${module}.ldif"
      cat "/etc/openldap.dist/modules/${module}.ldif" | sed -f /tmp/env_tmp > "$tmp_module_file"
      set +e
      output=$(slapadd -n0 -F /etc/openldap/slapd.d -l "$tmp_module_file" 2>&1)
      ret=$?
      set -e
      rm -f $tmp_module_file
      if [ $ret != 0 ] && [ $(echo ${output} | grep -c "already loaded") == 0 ]; then
        echo "Error: unable to load module '$module' - ${output}"
        exit 1
      fi
      echo "Loaded module '$module'."
    done
  fi
fi

#
# Boostrap data
#

if [ "$first_run" = "true" ] && [ -n "$SLAPD_LOAD_LDIFS" ]; then
  ldifs=$(echo "$SLAPD_LOAD_LDIFS" | sed 's/,$//')
  IFS=","
  for ldif in $ldifs; do
    ldif_file="/var/tmp/openldap/ldifs/${ldif}"
    echo "Rendering and applying LDIF template file '$ldif_file' ..."
    tmp_ldif_file="/tmp/${ldif}"
    cat "$ldif_file" | sed -f /tmp/env_tmp > "$tmp_ldif_file"
    set +e
    output=$(slapadd -n1 -F /etc/openldap/slapd.d -l "$tmp_ldif_file" 2>&1)
    ret=$?
    set -e
    rm -f $tmp_ldif_file
    if [ $ret != 0 ] && [ $(echo ${output} | grep -c "already exists") == 0 ]; then
      echo "Error : Unable to add records - ${output}"
      exit 1
    fi
    echo "Successfully applied LDIF template file '${ldif_file}' - ${output}."
  done
fi

# Delete injectable env-var configuration file
rm -f /tmp/env_tmp

# Configure file permissions
mkdir -p /run/openldap/
chown -R ldap:ldap /run/openldap/ /etc/openldap/slapd.d /var/lib/openldap/openldap-data/

exec "$@"
