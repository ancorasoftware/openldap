FROM alpine:3.10.0

SHELL ["/bin/sh", "-c"]

LABEL maintainer="Sam Goldmann <samuel.goldmann@ancorasoftware.com>"

ENV OPENLDAP_VERSION=2.4.47 \ 
    LOG_LEVEL=32768 \ 
    SECRETS_DIR=/run/secrets \
    ACCESS_CONTROL="access to * by * read" \
    SLAPD_PPOLICY_DN_PREFIX="cn=default,ou=policies" \
    SLAPPASSWD_SECRETS="SLAPD_PASSWORD,NEXUS_PASSWORD,JENKINS_PASSWORD"   

RUN set -x && \
    apk --no-cache add openldap openldap-back-mdb openldap-overlay-ppolicy openldap-clients && \
        mkdir -p /run/openldap /var/lib/openldap/openldap-data /etc/openldap/slapd.d

EXPOSE 389

# TODO: make data directory an env-var
VOLUME ["/var/lib/openldap/openldap-data"]

COPY resources/ldifs/ /var/tmp/openldap/ldifs
COPY resources/slapd.conf /etc/openldap.dist/
COPY resources/modules/ppolicy.ldif /etc/openldap.dist/modules/ppolicy.ldif
COPY resources/check_password.conf /etc/openldap.dist/check_password.conf
COPY resources/entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

#TODO: Make the log level an env-var
CMD ["slapd", "-d", "32768", "-u", "ldap", "-g", "ldap"]
