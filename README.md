# Ancora OpenLDAP: ancorasoftware/openldap

[![Docker Image Version (latest semver)](https://img.shields.io/docker/v/ancorasoftware/openldap)](https://hub.docker.com/repository/docker/ancorasoftware/openldap)

This is an [`alpine`](https://hub.docker.com/_/alpine)-base [OpenLDAP](https://www.openldap.org/) image used by Ancora DevOps.

## Using this image

This image can be launched as follows:

```shell
docker run --name <container-name> -d -p 389:389 ancorasoftware/openldap:<version>
```

### Volumes

This image stores data in the `/var/lib/openldap/openldap-data` directory. That should be made an explicit volume so that it can be managed and attached to another container for backup and upgrades:

```shell
docker run --name <your-container-name> -d -v $(pwd)/openldap-data:/var/lib/openldap/openldap-data -p 8081:8081 ancorasoftware/openldap:<version>
```

### Configuration 

This image supports the following environment variables:

| Variable                | Required | Default      | Description                                                                |
|-------------------------|----------|--------------|----------------------------------------------------------------------------|
| SLAPD_DOMAIN            | &#10003; | -            | Domain components. `dc=ldap,dc=example,dc=com`.                            |
| SLAPD_FORCE_RECONFIGURE |          | false        | Force reconfiguration of the service after the image has been initialized. |
| SLAPD_SECRETS_DIR       |          | /run/secrets | Secrets mount point                                                        |

TODO: Complete/update env-var table

### Secrets

In an effort to maximize security and conform to [best practices](https://diogomonica.com/2017/03/27/why-you-shouldnt-use-env-variables-for-secret-data/), this image assumes sensitive data (e.g.  passwords, certs, and keys) are specified using [Kubernetes](https://kubernetes.io/docs/concepts/configuration/secret/) or [Docker secrets](https://docs.docker.com/engine/swarm/secrets/). All secrets are exposed to the conainer using the mount point specified by the `SECRETS_DIR` environment variable. 

| Secret         | Required | Description                    |
|----------------|----------|--------------------------------|
| SLAPD_PASSWORD | &#10003; | Password for the `admin` user. |

## Security

This container currently lacks TLS/SSL support.

## License

Please view [license information](./LICENSE.txt) for the software contained in this image.

## Acknowledgments

The image was inspired by the following:
* [`tiredofit/docker-openldap`](https://github.com/tiredofit/docker-openldap)
* [`Accenture/adop-ldap`](https://github.com/Accenture/adop-ldap)
* [`dinkel/openldap`](https://hub.docker.com/r/dinkel/openldap/) and 
* [`acobaugh/openldap-alpine`](https://github.com/acobaugh/openldap-alpine)
* [`danielguerra/alpine-openldap`](https://github.com/danielguerra69/docker-openldap)
